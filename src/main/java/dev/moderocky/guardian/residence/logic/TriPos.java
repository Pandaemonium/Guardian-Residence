package dev.moderocky.guardian.residence.logic;


import com.moderocky.guardian.mask.api.MagicList;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

public class TriPos {

    private final int[] pos = new int[3];

    public TriPos() {
    }

    public TriPos(int x, int y, int z) {
        pos[0] = x;
        pos[1] = y;
        pos[2] = z;
    }

    public static TriPos of(Vector vector) {
        return new TriPos(vector.getBlockX(), vector.getBlockY(), vector.getBlockZ());
    }

    public static TriPos of(Location location) {
        return of(location.toVector());
    }

    public static TriPos of(BoundingBox box) {
        return of(box.getCenter());
    }

    public static TriPos of(Block block) {
        return of(block.getLocation().toVector());
    }

    public static TriPos of(Entity entity) {
        return of(entity.getLocation());
    }

    public TriPos up() {
        return up(1);
    }

    public TriPos up(int i) {
        return new TriPos(pos[0], pos[1] + i, pos[2]);
    }

    public TriPos down() {
        return down(1);
    }

    public TriPos down(int i) {
        return new TriPos(pos[0], pos[1] - i, pos[2]);
    }

    public TriPos add(Number x, Number y, Number z) {
        return new TriPos(pos[0] + (int) x, pos[1] + (int) y, pos[2] + (int) z);
    }

    public TriPos add(Number... num) {
        if (num.length < 3) throw new IllegalArgumentException("Number array must have 3+ values.");
        return new TriPos(pos[0] + (int) num[0], pos[1] + (int) num[1], pos[2] + (int) num[2]);
    }

    public void modify(Number x, Number y, Number z) {
        pos[0] = pos[0] + (int) x;
        pos[1] = pos[1] + (int) y;
        pos[2] = pos[2] + (int) z;
    }

    public void modify(Number... num) {
        if (num.length > 0)
            pos[0] = pos[0] + (int) num[0];
        if (num.length > 1)
            pos[1] = pos[1] + (int) num[1];
        if (num.length > 2)
            pos[2] = pos[2] + (int) num[2];
    }

    public MagicList<TriPos> cuboid(int diX, int diY, int diZ) {
        MagicList<TriPos> list = new MagicList<>();
        int startX = (pos[0] - (diX / 2)), startY = (pos[1] - (diY / 2)), startZ = (pos[2] - (diZ / 2));
        for (int x = 0; x < diX; x++) {
            for (int y = 0; y < diY; y++) {
                for (int z = 0; z < diZ; z++) {
                    list.add(new TriPos(x + startX, y + startY, z + startZ));
                }
            }
        }
        return list;
    }

    public MagicList<TriPos> rectangle(int diX, int diZ) {
        MagicList<TriPos> list = new MagicList<>();
        int startX = (pos[0] - (diX / 2)), startZ = (pos[2] - (diZ / 2));
        for (int x = 0; x < diX; x++) {
            for (int z = 0; z < diZ; z++) {
                list.add(new TriPos(x + startX, pos[1], z + startZ));
            }
        }
        return list;
    }

    public @NotNull Location getLocation(@NotNull World world) {
        return new Location(world, pos[0], pos[1], pos[2]);
    }

    public @NotNull Block getBlock(@NotNull World world) {
        return getLocation(world).getBlock();
    }

    public @NotNull BoundingBox getBox(TriPos triPos) {
        return new BoundingBox(pos[0], pos[1], pos[2], triPos.pos[0], triPos.pos[1], triPos.pos[2]);
    }

    public @NotNull Vector getVector(TriPos triPos) {
        return new Vector(triPos.pos[0] - pos[0], triPos.pos[1] - pos[1], triPos.pos[2] - pos[2]);
    }

}
