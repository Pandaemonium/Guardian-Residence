package dev.moderocky.guardian.residence.logic;

import com.moderocky.guardian.mask.api.MagicList;
import dev.moderocky.guardian.residence.Residence;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

public class Pyramid {

    public static final int MAX_LEVELS = 8;
    public static final Material BEACON = Residence.getInstance().config.beacon;
    public static final Material MATERIAL = Residence.getInstance().config.pyramid;
    private final Block cap;
    private int levels;

    public Pyramid(Block cap) {
        this.cap = cap;
        evaluate();
    }

    public boolean isValid() {
        if (cap.getType() != BEACON) return false;
        evaluate();
        return levels > 0;
    }

    public Block getCap() {
        return cap;
    }

    public MagicList<Block> getBlocks() {
        MagicList<Block> blocks = new MagicList<>();
        if (cap.getType() != BEACON) return blocks;
        World world = cap.getWorld();
        TriPos select = TriPos.of(cap);
        int lv = 3;
        levels = 0;
        for (int i = 0; i < 8; i++)
            check:{
                select = select.down();
                MagicList<Block> list = select.rectangle(lv, lv).collect(pos -> pos.getBlock(world));
                for (Material material : blocks.collect(Block::getType)) {
                    if (material != MATERIAL) break check;
                }
                blocks.addAll(list);
                levels++;
                lv += 2;
            }
        return blocks;
    }

    public int getLevels() {
        evaluate();
        return levels;
    }

    private void evaluate() {
        World world = cap.getWorld();
        TriPos select = TriPos.of(cap);
        int lv = 3;
        levels = 0;
        for (int i = 0; i < 8; i++)
            check:{
                select = select.down();
                for (Material material : select.rectangle(lv, lv).collect(pos -> pos.getBlock(world).getType())) {
                    if (material != MATERIAL) break check;
                }
                levels++;
                lv += 2;
            }
    }

}
