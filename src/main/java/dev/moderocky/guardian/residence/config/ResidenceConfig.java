package dev.moderocky.guardian.residence.config;

import com.moderocky.guardian.mask.annotation.Configurable;
import com.moderocky.guardian.mask.template.Config;
import dev.moderocky.guardian.residence.command.MenuType;
import org.bukkit.Material;
import org.jetbrains.annotations.NotNull;

public class ResidenceConfig implements Config {

    @Configurable
    @Configurable.Keyed("command_display_type")
    public MenuType type = MenuType.TEXT;

    @Configurable
    @Configurable.Keyed("home_beacon_material")
    public Material beacon = Material.BEACON;

    @Configurable
    @Configurable.Keyed("home_pyramid_material")
    public Material pyramid = Material.OBSIDIAN;

    @Override
    public @NotNull String getFolderPath() {
        return "plugins/Guardian/";
    }

    @Override
    public @NotNull String getFileName() {
        return "residence.yml";
    }

}
