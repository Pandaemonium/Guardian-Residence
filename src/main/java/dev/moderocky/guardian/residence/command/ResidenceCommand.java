package dev.moderocky.guardian.residence.command;

import com.moderocky.guardian.Guardian;
import com.moderocky.guardian.api.GuardianAPI;
import com.moderocky.guardian.api.Zone;
import com.moderocky.guardian.command.GuardianCommand;
import com.moderocky.guardian.mask.api.MagicList;
import com.moderocky.guardian.mask.command.ArgPlayer;
import com.moderocky.guardian.mask.command.Commander;
import com.moderocky.guardian.mask.template.WrappedCommand;
import com.moderocky.guardian.util.Messenger;
import dev.moderocky.guardian.residence.Residence;
import dev.moderocky.guardian.residence.config.ResidenceConfig;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.NamespacedKey;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ResidenceCommand extends Commander<Player> implements WrappedCommand {

    private final @NotNull Messenger messenger = Guardian.getMessenger();
    private final @NotNull GuardianAPI api = Guardian.getApi();
    private final @NotNull ResidenceConfig config = Residence.getInstance().config;

    @Override
    @SuppressWarnings("unchecked")
    public @NotNull Main create() {
        return command("residence").setSuper((Commander<? extends Player>) GuardianCommand.command)
                .arg("help", getDefault())
                .arg("list", config.type.list(), arg(config.type.listOther(), new ArgPlayer()));
    }

    @Override
    public @NotNull Consumer<Player> getDefault() {
        return sender -> messenger.sendMessage(api.getCommandHelpMessage(this), sender);
    }

    @Override
    public @NotNull List<String> getAliases() {
        return Arrays.asList("res", "gres", "guardres");
    }

    @Override
    public @NotNull String getUsage() {
        return "/" + getCommand();
    }

    @Override
    public @NotNull String getDescription() {
        return "The main command for managing Guardian-Residences.";
    }

    @Override
    public @Nullable String getPermission() {
        return "guardian.command.residence";
    }

    @Override
    public @Nullable String getPermissionMessage() {
        return null;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        if (!(commandSender instanceof Player))
            return false;
        return execute((Player) commandSender, strings);
    }
}
