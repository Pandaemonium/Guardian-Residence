package dev.moderocky.guardian.residence.command;

import com.moderocky.guardian.mask.command.Commander;
import com.moderocky.guardian.mask.template.WrappedCommand;
import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class HomeCommand extends Commander<Player> implements WrappedCommand {
    @Override
    public @NotNull Main create() {
        return command("home");
    }

    @Override
    public @NotNull Consumer<Player> getDefault() {
        return player -> {
            // TODO - Finish this another time :(
        };
    }

    @Override
    public @NotNull List<String> getAliases() {
        return Arrays.asList("reshome", "rhome", "ghome");
    }

    @Override
    public @NotNull String getUsage() {
        return "/" + getCommand();
    }

    @Override
    public @NotNull String getDescription() {
        return "Travel to your home residence.";
    }

    @Override
    public @Nullable String getPermission() {
        return "guardian.command.home";
    }

    @Override
    public @Nullable String getPermissionMessage() {
        return null;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player)) return false;
        return execute((Player) sender, args);
    }
}
