package dev.moderocky.guardian.residence.command;

import com.moderocky.guardian.Guardian;
import com.moderocky.guardian.api.GuardianAPI;
import com.moderocky.guardian.api.Zone;
import com.moderocky.guardian.mask.api.MagicList;
import com.moderocky.guardian.util.Messenger;
import dev.moderocky.guardian.residence.Residence;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.NamespacedKey;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.smartcardio.CommandAPDU;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public enum MenuType {

    GUI{
    },
    TEXT{
        @Override
        public Consumer<Player> help() {
            return null;
        }

        @Override
        public Consumer<Player> list() {
            return player -> {
                UUID uuid = player.getUniqueId();
                ComponentBuilder builder = new ComponentBuilder("Residences you can access:");
                for (NamespacedKey key : Residence.getInstance().getResZones(uuid).collect(Zone::getKey)) {
                    builder
                            .append(System.lineSeparator())
                            .reset()
                            .append(" - ")
                            .color(ChatColor.DARK_GRAY)
                            .append(api.getZone(key).getName())
                            .color(ChatColor.GRAY)
                            .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/zone info " + key.toString()));
                }
                messenger.sendMessage(builder.create(), player);
            };
        }

        @Override
        public BiConsumer<Player, Object[]> listOther() {
            return (sender, input) -> {
                UUID uuid = ((Player) input[0]).getUniqueId();
                MagicList<NamespacedKey> keys = Residence.getInstance().getResZones(uuid).collect(Zone::getKey);
                ComponentBuilder builder = new ComponentBuilder("Residences " + ((Player) input[0]).getName() + " can access:");
                for (NamespacedKey key : keys)
                    builder
                            .append(System.lineSeparator()).reset()
                            .append(" - ").color(ChatColor.DARK_GRAY)
                            .append(api.getZone(key).getName()).color(ChatColor.GRAY)
                            .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/zone info " + key.toString()));
                messenger.sendMessage(builder.create(), sender);
            };
        }
    };

    private static final GuardianAPI api = Guardian.getApi();
    private static final Messenger messenger = Guardian.getMessenger();

    MenuType() {

    }

    public abstract Consumer<Player> list();

    public abstract BiConsumer<Player, Object[]> listOther();
    public abstract BiConsumer<Player, Object[]> home();
    public abstract BiConsumer<Player, Object[]> create();
    public abstract BiConsumer<Player, Object[]> delete();

}
