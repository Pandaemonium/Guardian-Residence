package dev.moderocky.guardian.residence;

import com.moderocky.guardian.Guardian;
import com.moderocky.guardian.api.GuardianAPI;
import com.moderocky.guardian.api.Zone;
import com.moderocky.guardian.mask.template.BukkitPlugin;
import com.moderocky.guardian.mask.api.MagicList;
import com.moderocky.guardian.mask.template.Listener;
import dev.moderocky.guardian.residence.command.HomeCommand;
import dev.moderocky.guardian.residence.command.ResidenceCommand;
import dev.moderocky.guardian.residence.config.ResidenceConfig;
import dev.moderocky.guardian.residence.zone.ResidentialZone;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.List;
import java.util.UUID;

public class Residence extends BukkitPlugin {

    private static Residence instance;
    private final GuardianAPI api = Guardian.getApi();

    public final ResidenceConfig config = new ResidenceConfig();

    @Override
    public void startup() {
        instance = this;
        register(
                new ResidenceCommand(),
                new HomeCommand()
        );
    }

    @Override
    public void disable() {
        instance = null;
    }

    public static Residence getInstance() {
        return instance;
    }

    public MagicList<ResidentialZone> getResZones(UUID uuid) {
        MagicList<ResidentialZone> zones = new MagicList<>();
        for (Zone zone : api.getZones()) {
            if (zone instanceof ResidentialZone && zone.getAllowedPlayers().contains(uuid))
                zones.add((ResidentialZone) zone);
        }
        return zones;
    }

}
